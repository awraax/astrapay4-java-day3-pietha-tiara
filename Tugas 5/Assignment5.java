package Day3.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Assignment5 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Peter");
        list.add("John");
        list.add("Billy");
        list.add("Jack");
        list.remove("Peter");

        ArrayList<String> list1 = new ArrayList<>();
        list1.add("John");
        list1.add("Jack");
        list.retainAll(list1);

        Iterator itr = list.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }


    }
}
