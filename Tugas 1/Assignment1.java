package Day3.Task;

import java.util.Scanner;

public class Assignment1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Panjang Array Integer = ");
        int panjangArray = input.nextInt();

        int[] index = new int[panjangArray];

        for (int i=0; i < index.length; i++){
            System.out.print("Index [" +i+ "] =");
            index[i] = input.nextInt();
        }

        System.out.println("Panjang Array Integer = "+index.length);

        System.out.print("Array Integer [");

        for (int el: index){
            System.out.print(" " +el+ " ");
        }

        System.out.print("]");
    }
}
