package Day3.Task;

import java.util.Arrays;
import java.util.Scanner;

public class Assignment2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Baris Array Integer : ");
        int barisArray = input.nextInt();
        System.out.print("Kolom Array Integer : ");
        int kolomArray = input.nextInt();

        int[][] index = new int[barisArray][kolomArray];

        for (int i=0; i < index.length; ++i){
            for (int j=0; j < index[i].length; ++j){
                System.out.print("Index [" +i+ ", " +j+ "] = ");
                index[i][j] = input.nextInt();
            }
        }

        System.out.println("Baris dan Kolom Array 2 Dimensi adalah ["+barisArray+", "+kolomArray+"] ");

        System.out.println("Array multidimensi 2 Dimensi ");
        for (int i=0; i < index.length; i++){
            for (int j=0; j < index[i].length; j++){
                System.out.print(index[i][j] + "");
            }
            System.out.println(" ");
        }
        input.close();
    }
}
