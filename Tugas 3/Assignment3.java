package Day3.Task;
import java.util.Scanner;

public class Assignment3 {
    public static void bubbleSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++){
            for (int j = 0; j < arr.length - 1 - i; j++){
                if (arr[j + 1] < arr[j] ){
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] =temp;
                }
            }
        }
    }

    static void binarySearch(int[] arr, int target) {
        int left = 0;
        int middle;
        int right = arr.length - 1;
        while (left <= right) {
            middle = (left + right) / 2;
            if (arr[middle] == target) {
                System.out.println("Elemen ditemukan di indeks " +middle);
                break;
            } else if (arr[middle] < target) {
                left = middle + 1;
            } else if (arr[middle] > target) {
                right = middle - 1;
            }
        }

    }

    public static void main(String[] args) {
        int pilih = 0;
        int [] array;
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan panjang array = ");
        int panjangArray = input.nextInt();
        array = new int[panjangArray];
        while (pilih !=4){
            System.out.println("1. Input Array");
            System.out.println("2. Bubble Sort");
            System.out.println("3. Binary Search");
            System.out.println("4. Exit");
            System.out.print("Input nomor: ");
            pilih = input.nextInt();

            switch (pilih){
                case 1:
                    for (int i = 0; i < array.length; i++){
                        System.out.print("Angka array = ");
                        array[i] =input.nextInt();
                    }
                    for (int array1: array){
                        System.out.print(array1 + " ");
                    }
                    System.out.println();
                    break;
                case 2:
                    System.out.println("Hasil sort : ");
                    bubbleSort(array);
                    for (int array1: array){
                        System.out.print(array1 + " ");
                    }
                    System.out.println();
                    break;
                case 3:
                    System.out.print("Hasil pencarian : ");
                    int search =input.nextInt();
                    binarySearch(array, search);
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Tidak ada pilihan");
            }
        }
        input.close();
    }
}
